<img src="/client/src/assets/images/Logo.png" alt="Manĝi" height="100" width="150" />

# About the project
With Manĝi (Esperanto for "to eat") you can search for open restaurants currently delivering food.
## Features
- Search for restaurants by postcode or geolocation (you should use a VPN for this).
- Information about a given restaurant (website, food, rating, delivery time, etc.).
- Filter by food preference (click on food's name).

<img src="/client/src/assets/images/screenshots/screenshot_1.png" alt="Manĝi" />
<img src="/client/src/assets/images/screenshots/screenshot_2.png" alt="Manĝi" />
<img src="/client/src/assets/images/screenshots/screenshot_3.png" alt="Manĝi" />

## Usage
If you would like to run the application locally, first clone the repository
```
git clone https://gitlab.com/DrKillshot/mangi
cd mangi
```
In one terminal run the Node.js server
```
node app.js
```
and in another run the Angular development server
```
cd client
ng serve
```
Wait for the browser to open at `http://localhost:4200`.
## Tech Stack
The web application was developed using [Node.js](https://nodejs.org/en/) on the back-end and [Angular.js](https://angularjs.org/) on the front end. Node.js was only used for the purpose of sending requests to the API since we can't do it directly with Angular due to cors.

The [Just Eat](https://uk.api.just-eat.io/docs) API was used to retreive all the information.

[Tailwindcss](https://tailwindcss.com/) was used as the design framework and the logo was developed using Figma. You can check the project's Figma file [here](https://www.figma.com/file/c8UwimeGaDBFiSCAg9d1nn/Man%C4%9Di?node-id=0%3A1).  Additionally, [AOS animations](https://michalsnik.github.io/aos/) were used.

## Back-end API

### `/bypostcode/` (GET)
Retrieves list of restaurants given a postcode address.

#### Get parameters
- `postcode:string` - Post code address.

Example: `curl 'https://localhost:3000/postcode/e1'`

### `/bylatlong/` (GET)
Retrevies list of restaurants near a given latitude (`lat`) and longitude (`long`). These parameters are not inserted by the user but rather they're found using the browser's `navigator` object.
#### Get parameters
- `lat:number`: User latitude: corresponds to a number between -90 and 90.
- `long:number`: User longitude: corresponds number between -180 and 180.

Example: `curl 'https://localhost:3000/bylatlong/?lat=51.451772&long=-2.596847'`