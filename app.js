const express = require('express');
const app = express();
const port = `${process.env.PORT || 3000}`;
const cors = require('cors');
const axios = require('axios');

app.use(cors());
app.use(express.json());
app.use(express.static(process.cwd()+"/client/dist/frontend-test/"));

const base_url = "https://uk.api.just-eat.io";

// Endpoint for data given postcode
app.get('/bypostcode/:postcode', (req, res) => {
	const postcode = req.params.postcode;
	axios.get(`${base_url}/restaurants/bypostcode/${postcode}`).then((response) => {
		res.json(response.data);
	});
});

// Endpoint for data given latitude and longitude
app.get('/bylatlong/:lat/:long', (req, res) => {
	const { lat, long } = req.params;
	axios.get(`${base_url}/restaurants/bylatlong?latitude=${lat}&longitude=${long}`).then((response) => {
		res.send(response.data.postcode);
	});
});

app.get('/', (req,res) => {
  res.sendFile(process.cwd()+"/client/dist/frontend-test/index.html")
});

app.listen(port, () => {
  console.log(`App listening on port ${port}`)
});