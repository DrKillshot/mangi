import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Router, RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgxSpinnerModule } from "ngx-spinner";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { HomeHeroComponent } from './components/home-hero/home-hero.component';
import { IonicModule } from '@ionic/angular';
import { HeroSearchComponent } from './components/hero-search/hero-search.component';
import { HomeStatsComponent } from './components/home-stats/home-stats.component';
import { CostumerReviewsComponent } from './components/costumer-reviews/costumer-reviews.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeStepsComponent } from './components/home-steps/home-steps.component';
import { LandingComponent } from './pages/landing/landing.component';
import { SearchComponent } from './pages/search/search.component';
import { RestaurantCardComponent } from './components/restaurant-card/restaurant-card.component';
import { SkeletonComponent } from './components/skeleton/skeleton.component';

const appRoutes:Routes = [
 { path : '', component: LandingComponent },
 { path : 'search/:postcode', component: SearchComponent },
 { path : '**', component : LandingComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeHeroComponent,
    HeroSearchComponent,
    HomeStatsComponent,
    CostumerReviewsComponent,
    FooterComponent,
    HomeStepsComponent,
    LandingComponent,
    SearchComponent,
    RestaurantCardComponent,
    SkeletonComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    IonicModule.forRoot(),
    RouterModule.forRoot(appRoutes, { enableTracing: true }),
    BrowserAnimationsModule,
    NgxSpinnerModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
