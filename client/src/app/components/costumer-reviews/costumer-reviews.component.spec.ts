import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CostumerReviewsComponent } from './costumer-reviews.component';

describe('CostumerReviewsComponent', () => {
  let component: CostumerReviewsComponent;
  let fixture: ComponentFixture<CostumerReviewsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CostumerReviewsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CostumerReviewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
