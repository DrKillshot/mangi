import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-restaurant-card',
  templateUrl: './restaurant-card.component.html',
  styleUrls: ['./restaurant-card.component.css']
})
export class RestaurantCardComponent implements OnInit {

  @Input() logo!:string;
  @Input() url!:string;
  @Input() name!:string;
  @Input() rating!:string;
  @Input() totalVotes!:string;
  @Input() foods!:any;
  @Input() delivery!:any;
  @Input() fee!:string;
  @Input() promotions!:any;
  @Input() filteredFoods!:any;
  @Input() refreshFilter:any;

  addFoodFilter = (e:any) => {
    const food = e.target.value;
    if ( !this.filteredFoods.includes(food) ){
      this.filteredFoods.push(food);
      this.refreshFilter()
    }
  };

  constructor() { }

  ngOnInit(): void {
  }

}
