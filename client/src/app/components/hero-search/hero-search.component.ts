import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { LocationService } from '../../services/location.service';
import { RestaurantService } from '../../services/restaurant.service';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-hero-search',
  templateUrl: './hero-search.component.html',
  styleUrls: ['./hero-search.component.css']
})
export class HeroSearchComponent implements OnInit {

  displayMessage:boolean=false;

  constructor(private router : Router, 
              private locationService : LocationService, 
              private spinner: NgxSpinnerService,
              private restaurant: RestaurantService) { }

  searchRestaurant:any = (e:any) => {
    const query = e.target.value;
    if ( query.trim().length !== 0 ){
      this.router.navigate([`/search/${query}`]);
    }
  }

  getUserLocation = () => {
    this.spinner.show()
    this.locationService.getLocation().then((res:any) => {
       this.restaurant.getByLatLong(res.lat, res.long).subscribe((response:any) => {
         response == null ? this.displayMessage = true : this.router.navigate([`/search/${response.postcode}`]);
         this.spinner.hide();
       });
    });
  }

  ngOnInit(): void {
  }

}
