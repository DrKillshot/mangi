import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RestaurantService {

  constructor(private http: HttpClient) { }

  baseUrl = "http://localhost:3000";

  // Returns data given postcode
  getByPostCode:any = (postcode:string) => {
    return this.http.get(`${this.baseUrl}/bypostcode/${postcode}`);
  }

  // Returns data given user's longitude and latitude
  getByLatLong:any = (lat:number, long:number) => {
    return this.http.get(`${this.baseUrl}/bylatlong/${lat}/${long}`);
  }

}
