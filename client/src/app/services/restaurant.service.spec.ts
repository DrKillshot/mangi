import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { RestaurantService } from './restaurant.service';

describe('RestaurantService', () => {
  let service: RestaurantService;
  let httpMock:HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ]
    });
    service = TestBed.inject(RestaurantService);
    httpMock = TestBed.get(HttpTestingController);
    const request = httpMock.expectOne(`${service.baseUrl}/bypostcode/ec4m`);
    expect(request.request.method).toBe("GET");
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

});
