import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  constructor() { }

  // Get user location
  getLocation = () => {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition((response:any) => {
        resolve({long: response.coords.longitude, lat: response.coords.latitude});
      });
    });
  };

}
