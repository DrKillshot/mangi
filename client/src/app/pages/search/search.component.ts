import { Component, OnInit } from '@angular/core';
import { RestaurantService } from '../../services/restaurant.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  constructor(private service:RestaurantService, private route: ActivatedRoute) { }

  postcode:string="";
  data:any=[];
  openRestaurants:any=[];
  limit:number = 12;

  isLoading:boolean=true;

  filteredFoods:any = [];

  addLimit = () => {
    this.limit += 12;
  };

  refreshFilter = () => {
    if ( this.filteredFoods.length == 0 ){
      this.openRestaurants = this.data.Restaurants.filter((restaurant:any) => { return restaurant.IsOpenNow === true });
    }else{
      const ids:any = [];
      this.data.Restaurants.map((restaurant:any, i:any) => {
        let restaurantFoods = restaurant.Cuisines.map((c:any) => {return c.Name} );
        const check = restaurantFoods.map((c:any) => { return this.filteredFoods.includes(c)} );
        if ( check.reduce((partialSum:any, a:any) => partialSum + a, 0) == this.filteredFoods.length ) {
          ids.push(restaurant.Id);
        }
      });
      this.openRestaurants = this.data.Restaurants.filter((restaurant:any) => {
        return ids.includes(restaurant.Id) === true
      });
    }
  };

  removeFoodFilter = (food:string) => {
    if ( this.filteredFoods.includes(food) ){
      this.filteredFoods = this.filteredFoods.filter((ff:any) => { return ff != food });
      this.refreshFilter();
    }
  }

  ngOnInit(): void {

     this.route.params.subscribe((params:any) => {
       this.postcode = params.postcode;
       this.service.getByPostCode(params.postcode).subscribe((response:any) => {
         this.data = response;
         this.openRestaurants = this.data.Restaurants.filter((restaurant:any) => { return restaurant.IsOpenNow === true });
         this.isLoading=false;
       });
     });
  }

}
