import { __decorate } from "tslib";
import { Component } from '@angular/core';
let HeroSearchComponent = class HeroSearchComponent {
    constructor(router, locationService, spinner, restaurant) {
        this.router = router;
        this.locationService = locationService;
        this.spinner = spinner;
        this.restaurant = restaurant;
        this.displayMessage = false;
        this.searchRestaurant = (e) => {
            const query = e.target.value;
            if (query.trim().length !== 0) {
                this.router.navigate([`/search/${query}`]);
            }
        };
        this.getUserLocation = () => {
            this.spinner.show();
            this.locationService.getLocation().then((res) => {
                this.restaurant.getByLatLong(res.lat, res.long).subscribe((response) => {
                    response.postcode == null ? this.displayMessage = true : this.router.navigate([`/search/${response.postcode}`]);
                    this.spinner.hide();
                });
            });
        };
    }
    ngOnInit() {
    }
};
HeroSearchComponent = __decorate([
    Component({
        selector: 'app-hero-search',
        templateUrl: './hero-search.component.html',
        styleUrls: ['./hero-search.component.css']
    })
], HeroSearchComponent);
export { HeroSearchComponent };
//# sourceMappingURL=hero-search.component.js.map