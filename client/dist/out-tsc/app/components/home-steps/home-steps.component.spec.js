import { TestBed } from '@angular/core/testing';
import { HomeStepsComponent } from './home-steps.component';
describe('HomeStepsComponent', () => {
    let component;
    let fixture;
    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [HomeStepsComponent]
        })
            .compileComponents();
        fixture = TestBed.createComponent(HomeStepsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=home-steps.component.spec.js.map