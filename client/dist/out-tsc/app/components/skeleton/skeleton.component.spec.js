import { TestBed } from '@angular/core/testing';
import { SkeletonComponent } from './skeleton.component';
describe('SkeletonComponent', () => {
    let component;
    let fixture;
    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [SkeletonComponent]
        })
            .compileComponents();
        fixture = TestBed.createComponent(SkeletonComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=skeleton.component.spec.js.map