import { TestBed } from '@angular/core/testing';
import { HomeStatsComponent } from './home-stats.component';
describe('HomeStatsComponent', () => {
    let component;
    let fixture;
    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [HomeStatsComponent]
        })
            .compileComponents();
        fixture = TestBed.createComponent(HomeStatsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=home-stats.component.spec.js.map