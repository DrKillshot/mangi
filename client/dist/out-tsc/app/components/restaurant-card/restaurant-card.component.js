import { __decorate } from "tslib";
import { Component, Input } from '@angular/core';
let RestaurantCardComponent = class RestaurantCardComponent {
    constructor() { }
    ngOnInit() {
    }
};
__decorate([
    Input()
], RestaurantCardComponent.prototype, "logo", void 0);
__decorate([
    Input()
], RestaurantCardComponent.prototype, "name", void 0);
__decorate([
    Input()
], RestaurantCardComponent.prototype, "rating", void 0);
__decorate([
    Input()
], RestaurantCardComponent.prototype, "totalVotes", void 0);
__decorate([
    Input()
], RestaurantCardComponent.prototype, "foods", void 0);
__decorate([
    Input()
], RestaurantCardComponent.prototype, "delivery", void 0);
__decorate([
    Input()
], RestaurantCardComponent.prototype, "fee", void 0);
__decorate([
    Input()
], RestaurantCardComponent.prototype, "promotions", void 0);
RestaurantCardComponent = __decorate([
    Component({
        selector: 'app-restaurant-card',
        templateUrl: './restaurant-card.component.html',
        styleUrls: ['./restaurant-card.component.css']
    })
], RestaurantCardComponent);
export { RestaurantCardComponent };
//# sourceMappingURL=restaurant-card.component.js.map