import { TestBed } from '@angular/core/testing';
import { RestaurantCardComponent } from './restaurant-card.component';
describe('RestaurantCardComponent', () => {
    let component;
    let fixture;
    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [RestaurantCardComponent]
        })
            .compileComponents();
        fixture = TestBed.createComponent(RestaurantCardComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=restaurant-card.component.spec.js.map