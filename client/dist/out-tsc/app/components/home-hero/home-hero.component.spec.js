import { TestBed } from '@angular/core/testing';
import { HomeHeroComponent } from './home-hero.component';
describe('HomeHeroComponent', () => {
    let component;
    let fixture;
    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [HomeHeroComponent]
        })
            .compileComponents();
        fixture = TestBed.createComponent(HomeHeroComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=home-hero.component.spec.js.map