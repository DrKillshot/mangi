import { __decorate } from "tslib";
import { Component } from '@angular/core';
let SearchComponent = class SearchComponent {
    constructor(service, route) {
        this.service = service;
        this.route = route;
        this.postcode = "";
        this.data = [];
        this.openRestaurants = [];
        this.limit = 10;
        this.isLoading = true;
        this.addLimit = () => {
            this.limit += 10;
        };
    }
    ngOnInit() {
        this.route.params.subscribe((params) => {
            this.postcode = params.postcode;
            this.service.getByPostCode(params.postcode).subscribe((response) => {
                this.data = response;
                this.openRestaurants = this.data.Restaurants.filter((restaurant) => { return restaurant.IsOpenNow === true; });
                this.isLoading = false;
            });
        });
    }
};
SearchComponent = __decorate([
    Component({
        selector: 'app-search',
        templateUrl: './search.component.html',
        styleUrls: ['./search.component.css']
    })
], SearchComponent);
export { SearchComponent };
//# sourceMappingURL=search.component.js.map