import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
let RestaurantService = class RestaurantService {
    constructor(http) {
        this.http = http;
        this.baseUrl = "http://localhost:3000";
        // Returns data given postcode
        this.getByPostCode = (postcode) => {
            return this.http.get(`${this.baseUrl}/bypostcode/${postcode}`);
        };
        // Returns data given user's longitude and latitude
        this.getByLatLong = (lat, long) => {
            return this.http.get(`${this.baseUrl}/bylatlong/${lat}/${long}`);
        };
    }
};
RestaurantService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], RestaurantService);
export { RestaurantService };
//# sourceMappingURL=restaurant.service.js.map