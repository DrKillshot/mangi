import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
let LocationService = class LocationService {
    constructor() {
        // Get user location
        this.getLocation = () => {
            return new Promise((resolve, reject) => {
                navigator.geolocation.getCurrentPosition((response) => {
                    resolve({ long: response.coords.longitude, lat: response.coords.latitude });
                });
            });
        };
    }
};
LocationService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], LocationService);
export { LocationService };
//# sourceMappingURL=location.service.js.map