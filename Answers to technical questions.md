### Answers to technical questions.md

## Question 1
*How long did you spend on the coding test?*

Spent on average 4.5h/5h a day. Spent around 2h on design (plus 15min on branding even though I assume wasn't all that necessary) and an additional hour at the end to make the website responsive and added a few animations to the landing page. The rest of the time was spent on learning the language and implementing what I had learned.

*What would you add to your solution if you had more time?*

I quickly became dissatisfied with the way I structured the files. I created two folders for components: `pages` and `components`. The components in `pages` were the ones being rendered with Angular's Router and they contained components from the folder `components`. However, I don't think it makes a lot of sense adding a component for the landing page hero in `components` as it will only be used on one page. The `components` folder should be used only for components shared by most pages (buttons, messages, cards, etc.). Probably, what I would do, would be to move page-specific components, say the landing page's hero, to the folder `pages/landing/landing-hero` and have the main component in `/pages/landing`. You may also notice I didn't generate any small components (i.e., buttons, etc.) since this was a small project. Nevertheless, this is something worth doing.

In terms of functionalities, there is plenty of room for improvement since Just Eat has a very extensive API. Here are some ideas:

- Restaurant profiles: if you wanted more information on a given restaurant, we could have a simple profile page with additional information like location on the map, menus, deal groups.
- Place, track and receive orders (just eat allows you to place orders as a test)
- Allow users to have favorite restaurants (this of course would be better for the back-end, but we could use the localStorage to keep track with the front-end alone ). This could be achieved with a "Follow" button or simply keep track of the top restaurants the users order more from.
- Search directly by food: this is probably an important one since users more often than not, already have an idea of what they want.

## Question 2
*What was the most useful feature that was added to the latest version of your chosen language? Please include a snippet of code that shows how you've used it.*

Being new to Angular I have no idea about previous features. The syntax seems to be cleaner though (compared to older version code snippets I've seen from stackoverflow).

## Question 3
*How would you track down a performance issue in production? Have you ever had to do this?*

I've never had to do this, but I guess that for loading problems I'd be tempted to inspect the JS bundles generated in the production build. Network performance and website auditing also come to mind.